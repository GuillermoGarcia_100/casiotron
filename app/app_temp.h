/**------------------------------------------------------------------------------------------------
Brief.- app_temp.h
MCP9808 Temperature Sensor.
-------------------------------------------------------------------------------------------------*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _APP_TEMP_H_
#define _APP_TEMP_H_

/* Includes ------------------------------------------------------------------*/

#include "app_bsp.h"

/* Exported typedef -----------------------------------------------------------*/

typedef struct
{
    I2C_HandleTypeDef  	*I2cHandler;  /*!< I2C configure handler >*/
    
    GPIO_TypeDef		*AlertPort;  /*!< GPIO configure handler >*/
    
    uint32_t			AlertPin;  /*!< GPIO PIN configure      >*/

}TEMP_HandleTypeDef;


/* Exported define ------------------------------------------------------------*/
/* Exported macro -------------------------------------------------------------*/
/* Exported variables ---------------------------------------------------------*/
/* Exported functions prototypes ----------------------------------------------*/

void MOD_TEMP_Init(TEMP_HandleTypeDef *htemp);
void MOD_TEMP_MspInit(TEMP_HandleTypeDef *htemp);
uint16_t MOD_TEMP_Read(TEMP_HandleTypeDef *htemp);
void MOD_TEMP_SetAlarms(TEMP_HandleTypeDef *htemp, uint16_t lower, uint16_t upper);
void MOD_TEMP_DisableAlarm(TEMP_HandleTypeDef *hlcd);
uint16_t MOD_TEMP_GetAlarm_Upper(TEMP_HandleTypeDef *htemp);
uint16_t MOD_TEMP_GetAlarm_Lower(TEMP_HandleTypeDef *htemp);

/*-----------------------------------------------------------------------------*/

#endif