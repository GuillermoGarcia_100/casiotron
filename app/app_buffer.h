/**------------------------------------------------------------------------------------------------
Brief.- buffer.h
Buffer Circular
-------------------------------------------------------------------------------------------------*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _APP_BUFFER_H_
#define _APP_BUFFER_H_

/* Includes ------------------------------------------------------------------*/

#include "app_bsp.h"

/* Exported typedef -----------------------------------------------------------*/

typedef struct
{
    uint8_t  	*Buffer;   /*!< Buffer declarade array >*/

    uint32_t	Elements;  /*!< Elements the array    >*/

    uint32_t	Head;      /*!< Variable Head WRITE in buffer >*/

    uint32_t	Tail;      /*!< Variable Tail READ in buffer >*/

    uint8_t	    Empty;     /*!< Variable buffer is empty >*/

    uint8_t	    Full;      /*!< Variable buffer is Full >*/

}BUFFER_HandleTypeDef;

/* Exported define ------------------------------------------------------------*/
/* Exported macro -------------------------------------------------------------*/
/* Exported variables ---------------------------------------------------------*/
/* Exported functions prototypes ----------------------------------------------*/

void HIL_BUFFER_Init(BUFFER_HandleTypeDef *hbuffer);
void HIL_BUFFER_Write(BUFFER_HandleTypeDef *hbuffer, uint8_t data);
uint8_t HIL_BUFFER_Read(BUFFER_HandleTypeDef *hbuffer);
uint8_t HIL_BUFFER_IsEmpty(BUFFER_HandleTypeDef *hbuffer);

/*-----------------------------------------------------------------------------*/

#endif