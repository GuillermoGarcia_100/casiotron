/**------------------------------------------------------------------------------------------------
Brief.- CASIOTRON 5000 
State machine for receiving UART data
-------------------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

#include "app_serial.h"
#include "app_queue.h"
#include <string.h>
#include <ctype.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/*   Machine States   */
#define WAITING_DATA       0
#define BACKUP_DATA        1
#define VALID_COMMAND_AT   2 
#define REPORT_OK          3 
#define REPORT_ERROR       4  

/*    Maximum value the buffer RX */
#define MAX_CHAR           19

/* Private function prototypes -----------------------------------------------*/

/*  funtics Machine States   */
static void Waiting_Data(void);
static void Backup_Data(void);
static void Valid_command_AT(void);
static void Report_ERROR(void);
static void Report_OK(void);


static int32_t str_to_int(uint8_t const *str);
static void Valid_parameter_TIME(uint8_t *const parameter);
static void Valid_parameter_DATE(uint8_t *const parameter);
static void Valid_parameter_ALARM(uint8_t *const parameter);
static void Valid_parameter_TEMP(uint8_t *const parameter);
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static uint8_t buffer_RX[MAX_CHAR];
static uint8_t buffer_temp[MAX_CHAR];
static uint8_t RxByte;
static uint8_t *ptr_comandAT;
static uint8_t *ptr_param;
static uint32_t tickstart = 0;
static __IO ITStatus stat_IT_UART = RESET;
static uint8_t state = WAITING_DATA;

/*        Buffer Waiting Queue     */
static uint8_t SerialBuffer[150];
static QUEUE_HandleTypeDef Queue1_Handler;

SERIAL_MsgTypeDef Task_buffer[10];
static SERIAL_MsgTypeDef  MsgToSend;
QUEUE_HandleTypeDef Queue2_Handler;


static void (*Ptr_fun_Valid_parameter[])(uint8_t *const parameter) = {
    Valid_parameter_TIME, Valid_parameter_DATE, Valid_parameter_ALARM, Valid_parameter_TEMP };

static void (*Ptr_fun_Machine[])(void) = { Waiting_Data, Backup_Data, 
Valid_command_AT, Report_ERROR, Report_OK };
/*----------------------------------------------------------------------------*/

/*/**---------------------------------------------------------------
Brief.- Function Initialization the UART peripheral 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void serial_init(void)
{

     UartHandle.Init.BaudRate      = 115200;
     UartHandle.Init.WordLength    = UART_WORDLENGTH_8B;
     UartHandle.Init.StopBits      = UART_STOPBITS_1;
     UartHandle.Init.Parity        = UART_PARITY_NONE;
     UartHandle.Init.HwFlowCtl     = UART_HWCONTROL_NONE;
     UartHandle.Init.Mode          = UART_MODE_TX_RX;
     UartHandle.Instance           = USART2;
     UartHandle.Init.OverSampling  = UART_OVERSAMPLING_16;

    HAL_UART_Init(&UartHandle);
     
     /*   Initialitation Queue 1 serial_task  */
     Queue1_Handler.Buffer   = (void *)SerialBuffer;
     Queue1_Handler.Elements = 150;
     Queue1_Handler.Size     = sizeof(uint8_t);

    HIL_QUEUE_Init(&Queue1_Handler);
    
     /*   Initialitation Queue 2 serial_task and clock_task  */
     Queue2_Handler.Buffer   = (void *)Task_buffer;
     Queue2_Handler.Elements = 10;
     Queue2_Handler.Size     = sizeof(SERIAL_MsgTypeDef);

    HIL_QUEUE_Init(&Queue2_Handler);

     tickstart = HAL_GetTick();

     HAL_UART_Receive_IT(&UartHandle, &RxByte, 1);

}


/*/**---------------------------------------------------------------
Brief.- Serial state machine for serial, UART peripheral 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void serial_task(void)
{

     Ptr_fun_Machine[state]();

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Hours 1 at 12) (Minutes 0 at 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Waiting_Data(void)
{

     uint8_t aux = 0;
     static uint8_t index = 0;
    
     if ( (HAL_GetTick() - tickstart) >= 10)
     {
         tickstart = HAL_GetTick();

         if (stat_IT_UART)
         {
             stat_IT_UART = RESET;

             while ( HIL_QUEUE_IsEmpty(&Queue1_Handler) == 0 )
             {   
                 HAL_NVIC_DisableIRQ(USART2_IRQn);
                 HIL_QUEUE_Read(&Queue1_Handler, &aux);
                 HAL_NVIC_EnableIRQ(USART2_IRQn);

             if (aux == '\r')
             {
                 buffer_RX[index] = '\0';
                 index = 0;
                 state = BACKUP_DATA;
                 break;
             }
             else
             {
                 buffer_RX[index] = aux;
                 index++;
                 index %= MAX_CHAR;
             }
    
             }

         }

     }

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Hours 1 at 12) (Minutes 0 at 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Backup_Data(void)
{

     strcpy((char *)buffer_temp, (const char *)buffer_RX);

     ptr_comandAT = (uint8_t *)strtok((char *)buffer_temp, "=");

     ptr_param = (uint8_t *)strtok(NULL, "=");

     state = VALID_COMMAND_AT;

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Hours 1 at 12) (Minutes 0 at 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Valid_command_AT(void)
{

     if ((ptr_comandAT != NULL) && (ptr_param != NULL))
     {
        if (strcmp((const char *)ptr_comandAT, "AT+TIME") == 0)
        {
             Ptr_fun_Valid_parameter[TIME](ptr_param);
        }
        else if (strcmp((const char *)ptr_comandAT, "AT+DATE") == 0)
        {
             Ptr_fun_Valid_parameter[DATE](ptr_param);
        }
        else if (strcmp((const char *)ptr_comandAT, "AT+ALARM") == 0)
        {
             Ptr_fun_Valid_parameter[ALARM](ptr_param);
        }
        else if (strcmp((const char *)ptr_comandAT, "AT+TEMP") == 0)
        {
             Ptr_fun_Valid_parameter[TEMP](ptr_param);
        }
        else
        {
             state = REPORT_ERROR;
        }
     }
     else
     {
              state = REPORT_ERROR;
     }

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Hours 1 at 12) (Minutes 0 at 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Report_ERROR(void)
{

     HAL_UART_Transmit(&UartHandle, (uint8_t *)"OK\n", (sizeof("OK\n") - 1), 35);

     state = WAITING_DATA;

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Hours 1 at 12) (Minutes 0 at 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Report_OK(void)
{

     HAL_UART_Transmit(&UartHandle, (uint8_t *)"ERROR\n", (sizeof("ERROR\n") - 1), 35);

     state = WAITING_DATA;

}


/*/**---------------------------------------------------------------
Brief.- Convert string at int number  (Negative, Positive)  
Param.- <str> string number
Param.- <void> None
Return.- Convert whole number
----------------------------------------------------------------*/
static int32_t str_to_int(uint8_t const *str)
{

     uint8_t Flag_number_negative_1 = 0;
     int32_t number = 0;

     for (; *str != '\0'; ++str)
     {
        if (isdigit(*str) != 0)
        {
             number *= 10;
             number += *str - '0';
        }
        else if (*str == '-' && Flag_number_negative_1 == 0)
        {
             Flag_number_negative_1 = 1;
        }
        else if (*str != ' ')
        {
             return -1; //No es un numero
        }
     }

     if (Flag_number_negative_1)
     {
         number *= (-1);
     }  

     return number;

}


/*/**---------------------------------------------------------------
Brief.- Valid time parameters (Hours 1 to 12) (Minutes 1 to 59) (Secons 0 to 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Valid_parameter_TIME(uint8_t *const parameter)
{
     uint8_t *ptr_Hours, *ptr_Minutes, *ptr_Secods;
     int8_t Hours = -1, Minutes = -1, Secods = -1;

     ptr_Hours = (uint8_t *) strtok((char *)parameter, ",");

     ptr_Minutes = (uint8_t *) strtok(NULL, ",");

     ptr_Secods = (uint8_t *) strtok(NULL, ",");

     if (ptr_Hours != NULL)
     {
         Hours = str_to_int(ptr_Hours);
     }

     if (ptr_Minutes != NULL)
     {
         Minutes = str_to_int(ptr_Minutes);
     }

     if (ptr_Secods != NULL)
     {
         Secods = str_to_int(ptr_Secods);
     }

     if (((Hours == -1) || (Minutes == -1) || (Secods == -1)) ||
        (!(Hours >= 0 && Hours <= 23) || !(Minutes >= 0 && Minutes <= 59) || !(Secods >= 0 && Secods <= 59)))
     {
         state = REPORT_ERROR;
     }
     else
     {
         MsgToSend.msg = TIME;
         MsgToSend.param1 = Hours;
         MsgToSend.param2 = Minutes;
         MsgToSend.param3 = Secods;

         HIL_QUEUE_Write(&Queue2_Handler, &MsgToSend);

         state = REPORT_OK;
     }

}


/*/**---------------------------------------------------------------
Brief.- Valid date parameters (Date 1 at 31) (Month 1 at 12) (Year 0 at 99)
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Valid_parameter_DATE(uint8_t *const parameter)
{

     uint8_t *ptr_Date, *ptr_Month, *ptr_Year;
     int8_t Date = -1, Month = -1, Year = -1;

     ptr_Date = (uint8_t *) strtok((char *)parameter, ",");

     ptr_Month = (uint8_t *) strtok(NULL, ",");

     ptr_Year = (uint8_t *) strtok(NULL, ",");

     if (ptr_Date != NULL)
     {
          Date = str_to_int(ptr_Date);
     }

     if (ptr_Month != NULL)
     {
         Month = str_to_int(ptr_Month);
     }

     if (ptr_Year != NULL)
     {
         Year = str_to_int(ptr_Year);
     }

     if (((Date == -1) || (Month == -1) || (Year == -1)) ||
        (!(Date >= 1 && Date <= 31) || !(Month >= 1 && Month <= 12) || !(Year >= 0 && Year <= 99)))
     {
          state = REPORT_ERROR;
     }
     else
     {
         MsgToSend.msg = DATE;
         MsgToSend.param1 = Date;
         MsgToSend.param2 = Month;
         MsgToSend.param3 = Year;

         HIL_QUEUE_Write(&Queue2_Handler, &MsgToSend);

         state = REPORT_OK;
     }

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Hours 1 at 12) (Minutes 0 at 59) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Valid_parameter_ALARM(uint8_t *const parameter)
{

     uint8_t *ptr_Hours, *ptr_Minutes;
     int8_t Hours = -1, Minutes = -1;

     ptr_Hours = (uint8_t *) strtok((char *)parameter, ",");
     ptr_Minutes = (uint8_t *) strtok(NULL, ",");

     if (ptr_Hours != NULL)
     {
         Hours = str_to_int(ptr_Hours);
     }

     if (ptr_Minutes != NULL)
     {
         Minutes = str_to_int(ptr_Minutes);
     }

     if (((Hours == -1) || (Minutes == -1)) || (!(Hours >= 0 && Hours <= 23) || !(Minutes >= 0 && Minutes <= 59)))
     {
         state = REPORT_ERROR;
     }
     else
     {
         MsgToSend.msg = ALARM;
         MsgToSend.param1 = Hours;
         MsgToSend.param2 = Minutes;

         HIL_QUEUE_Write(&Queue2_Handler, &MsgToSend);

         state = REPORT_OK;
     }

}


/*/**---------------------------------------------------------------
Brief.- Valid alarm parameters (Lower -20 to 100 °C) (Upper -20 to 100 °C ) 
Param.- <parameter> Pointer const a parameter
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Valid_parameter_TEMP(uint8_t *const parameter)
{

     uint8_t *ptr_lower, *ptr_upper;
     int8_t T_Lower = -1, T_Upper = -1;

     ptr_lower = (uint8_t *) strtok((char *)parameter, ",");
     ptr_upper = (uint8_t *) strtok(NULL, ",");

      if (ptr_lower != NULL)
      {
         T_Lower = str_to_int(ptr_lower);
      }

      if (ptr_upper != NULL)
      {
         T_Upper = str_to_int(ptr_upper);
      }
      
      if (((T_Lower == -1) || (T_Upper == -1)) || (!(T_Lower >= (-20) && T_Lower <= 100) || !(T_Upper >= (-20) && T_Upper <= 100)))
      {
         state = REPORT_ERROR;
      }
      else
      {
         MsgToSend.msg = TEMP;
         MsgToSend.param1 = T_Lower;
         MsgToSend.param2 = T_Upper;

         HIL_QUEUE_Write(&Queue2_Handler, &MsgToSend);

         state = REPORT_OK;
      }
      
}

/*/**---------------------------------------------------------------
Brief.- Function Callback the peripheral UART Complete reception
Param.- <huart> typedef struct __UART_HandleTypeDef UART_HandleTypeDef
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

     HIL_QUEUE_Write(&Queue1_Handler, &RxByte);
  
     stat_IT_UART = SET;
  
     HAL_UART_Receive_IT(&UartHandle, &RxByte, 1);

}
