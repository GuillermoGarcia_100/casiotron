#include "app_bsp.h"
extern RTC_HandleTypeDef Rtc_Init;
extern UART_HandleTypeDef UartHandle;

/*/**---------------------------------------------------------------
Brief.- This function handles NMI(Non Maskable Interrupt) exception.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void NMI_Handler(void)
{
}

/*/**---------------------------------------------------------------
Brief.- This function handles HardFault exception.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HardFault_Handler(void)
{
    assert_param(0u);
}

/*/**---------------------------------------------------------------
Brief.- This function handles SVCall exception.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void SVC_Handler(void)
{
}

/*/**---------------------------------------------------------------
Brief.- This function handles PendSV exception.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void PendSV_Handler(void)
{
}

/*/**---------------------------------------------------------------
Brief.- This function handles SysTick exception.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void SysTick_Handler(void)
{
    HAL_IncTick();
}

/*/**---------------------------------------------------------------
Brief.- This function handles RTC interruption.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void RTC_IRQHandler(void)
{
    HAL_RTC_AlarmIRQHandler(&Rtc_Init);
}

/*/**---------------------------------------------------------------
Brief.- This function handles USART-UART interruption.
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void USART2_IRQHandler(void)
{
    HAL_UART_IRQHandler(&UartHandle);
}