/**------------------------------------------------------------------------------------------------
Brief.- queue.c
Waiting queue to communicate tasks
-------------------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
#include "app_queue.h"
#include <string.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/*----------------------------------------------------------------------------*/

/*/*---------------------------------------------------------------
Brief.- Initializes the Waiting Queue , tail, empty at 0.
Param.- <hqueue>  pointer to QUEUE_HandleTypeDef
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HIL_QUEUE_Init(QUEUE_HandleTypeDef *hqueue)
{

  hqueue->Head  = 0; 
  hqueue->Tail  = 0;
  hqueue->Empty = 1;
  hqueue->Full  = 0;

}


/*/*---------------------------------------------------------------
Brief.- Write a piece of information in the queue
Param.- <hqueue>  pointer to QUEUE_HandleTypeDef
Param.- <data> pointer void to data 
Return.- 1 -> Write complete, 0 -> Write not complete
----------------------------------------------------------------*/
uint8_t HIL_QUEUE_Write(QUEUE_HandleTypeDef *hqueue, void *data)
{
  
  if(hqueue->Full == 0)
  {
    memcpy( (hqueue->Buffer + (hqueue->Head * hqueue->Size)), data, hqueue->Size);
    hqueue->Head++;
    hqueue->Head %= hqueue->Elements;
    hqueue->Empty = 0;
      
    if(hqueue->Head == hqueue->Elements)
    {
      hqueue->Full = 1;
    }
      return 1;
      
  }
    return 0;
    
}


/*/*---------------------------------------------------------------
Brief.- Read a piece of information in the Queue
Param.- <hqueue>  pointer to QUEUE_HandleTypeDef
Param.- <data> pointer to data
Return.- 1 -> Read complete  0 -> Read not complete
----------------------------------------------------------------*/
uint8_t HIL_QUEUE_Read(QUEUE_HandleTypeDef *hqueue, void *data)
{
    
  if(hqueue->Empty == 1)
  {
    return 0;
  }
  else
  {
    memcpy(data, (hqueue->Buffer + (hqueue->Tail * hqueue->Size)), hqueue->Size );
    hqueue->Tail++;
    hqueue->Tail %= hqueue->Elements;
    
    if(hqueue->Full == 1)
    {
      hqueue->Full = 0;
    }
    
    if(hqueue->Tail == hqueue->Head)
    {
      hqueue->Empty = 1;
    }
    
    return 1;

  }

}


/*/*---------------------------------------------------------------
Brief.- The function returns a one if there are no more elements 
    that can be read from the Queue
Param.- <hqueue>  pointer to QUEUE_HandleTypeDef
Param.- <void> None
Return.- 1 -> Not Empty, 0 -> Empty
----------------------------------------------------------------*/
uint8_t HIL_QUEUE_IsEmpty(QUEUE_HandleTypeDef *hqueue)
{

  return hqueue->Empty;

}


