/**------------------------------------------------------------------------------------------------
Brief.- buffer.c
Buffer Circular
-------------------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
#include "app_buffer.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/*----------------------------------------------------------------------------*/

/*/*---------------------------------------------------------------
Brief.- Initializes the buffer circular head, tail, empty at 0.
Param.- <hbuffer>  pointer to BUFFER_HandleTypeDef
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HIL_BUFFER_Init(BUFFER_HandleTypeDef *hbuffer)
{

  hbuffer->Head  = 0;
  hbuffer->Tail  = 0;
  hbuffer->Empty = 1;
  hbuffer->Full  = 0;
  
}


/*/*---------------------------------------------------------------
Brief.- Write new 8-bit data to buffer if space is available
Param.- <hbuffer>  pointer to BUFFER_HandleTypeDef struct
Param.- <data> Data at write in buffer circular
Return.- void None
----------------------------------------------------------------*/
void HIL_BUFFER_Write(BUFFER_HandleTypeDef *hbuffer, uint8_t data)
{
    
  if(hbuffer->Full == 0)
  {
    hbuffer->Buffer[hbuffer->Head] = data;
    hbuffer->Head++;
    hbuffer->Head %= hbuffer->Elements;
    hbuffer->Empty = 0;

    if(hbuffer->Head == hbuffer->Tail)
    {
      hbuffer->Full = 1;
    }
  }
    
}


/*/*---------------------------------------------------------------
Brief.- Read a data from the buffer, the data that is read will no
  longer exist in the buffer.
Param.- <hbuffer>  pointer to BUFFER_HandleTypeDef
Param.- <void> None
Return.- The data Read de Circular buffer
----------------------------------------------------------------*/
uint8_t HIL_BUFFER_Read(BUFFER_HandleTypeDef *hbuffer)
{
    
  uint8_t aux = 0;

  if(hbuffer->Empty == 1) //If the buffer is empty
  {
    return aux;
  }
  else
  {
    aux = hbuffer->Buffer[hbuffer->Tail];
    hbuffer->Tail++;
    hbuffer->Tail %= hbuffer->Elements;
    
    if(hbuffer->Full == 1)
    {
      hbuffer->Full = 0;
    }
    
    if(hbuffer->Tail == hbuffer->Head)
    {
      hbuffer->Empty = 1;
    }

    return aux;
  }
  
}


/*/*---------------------------------------------------------------
Brief.- The function returns a one if there are no more elements 
  that can be read from the circular buffer
Param.- <hbuffer>  pointer to BUFFER_HandleTypeDef
Param.- <void> None
Return.- 1 -> Not Empty, 0 -> Empty
----------------------------------------------------------------*/
uint8_t HIL_BUFFER_IsEmpty(BUFFER_HandleTypeDef *hbuffer)
{

  return hbuffer->Empty;

}