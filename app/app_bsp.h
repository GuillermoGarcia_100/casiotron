#ifndef _BSP_H_
#define _BSP_H_

/* Includes ------------------------------------------------------------------*/

#include "stm32f0xx.h"
#include "app_temp.h"
#include <stdint.h>

/* Exported typedef -----------------------------------------------------------*/

typedef struct
{
	uint8_t msg;	 // type the Messenger
	uint8_t param1;	 // Hours o Day , Temperature Lower
    uint8_t param2;	 // Minutes o Month, Temperature Upper
   uint16_t param3; // Secons o Year

}SERIAL_MsgTypeDef;

/* Exported define ------------------------------------------------------------*/

#define TIME  0
#define DATE  1 
#define ALARM 2
#define TEMP  3
#define NONE  4

/* Exported macro -------------------------------------------------------------*/
/* Exported variables ---------------------------------------------------------*/
/* Exported function prototypes -----------------------------------------------*/

#endif

