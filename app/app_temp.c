/**------------------------------------------------------------------------------------------------
Brief.- app_temp.c
MCP9808 Temperature Sensor.
-------------------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

#include "app_temp.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/*  Address Divice  MCP9808  Addres code = 0011 + 111 + Wite -> 0011 1110 -> 0x3E  */
#define TEMP_ADDRESS_DIVICE          0x3E

/*  Pointer to register MCP9808  R= Readable bit, W= Writable bit  */
#define TEMP_P_REG_CONFIG              0x01  /*!< Access the Configuration register             R-W >*/ 
#define TEMP_P_REG_TEMPERATURE_UPPER   0x02  /*!< Access the Alert Temperature Upper register   R-W >*/
#define TEMP_P_REG_TEMPERATURE_LOWER   0x03  /*!< Access the Alert Temperature Lower register   R-W >*/
#define TEMP_P_REG_TEMPERATURE_CRIT    0x04  /*!< Access the Critical Temperature Trip register R-W >*/
#define TEMP_P_REG_TEMPERATURE_AMBIE   0x05  /*!< Access the Temperature ambient  register      R   >*/
#define TEMP_P_REG_MANUFACTURE_ID      0x06  /*!< Access the Manufacturer ID register           R   >*/
#define TEMP_P_REG_DIVICE_ID           0x07  /*!< Access the Device ID/Revision register        R   >*/
#define TEMP_P_REG_RESOLUTION          0x08  /*!< Access the Resolution register                R-W >*/

/*  Configuration Register #define TEMP_P_REG_CONFIG */
#define TEMP_REG_CONFIG_SHUTDOWN       0x0100  /*!< Shutdown Mode bit                   R-W >*/
#define TEMP_REG_CONFIG_CRITLOCKED     0x0080  /*!< TCRIT Lock bit                      R-W >*/
#define TEMP_REG_CONFIG_WINLOCKED      0x0040  /*!< TUPPER and TLOWER Window Lock bit   R-W >*/
#define TEMP_REG_CONFIG_INTCLR         0x0020  /*!< Interrupt Clear bit                 R-W >*/
#define TEMP_REG_CONFIG_ALERTSTAT      0x0010  /*!< Alert Output Status bit             R-W >*/
#define TEMP_REG_CONFIG_ALERTCTRL      0x0008  /*!< Alert Output Control bit           R-W >*/
#define TEMP_REG_CONFIG_ALERTSEL       0x0004  /*!< Alert Output Select bit             R-W >*/
#define TEMP_REG_CONFIG_ALERTPOL       0x0002  /*!< Alert Output Polarity bit           R-W >*/
#define TEMP_REG_CONFIG_ALERTMODE      0x0001  /*!< Alert Output Mode bit               R-W >*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

static uint16_t MOD_TEMP_READ_16BIT(TEMP_HandleTypeDef *htemp, uint8_t reg);
static void MOD_TEMP_WRITE_16BIT(TEMP_HandleTypeDef *htemp, uint8_t reg, uint16_t data);

/*----------------------------------------------------------------------------*/


/*/*---------------------------------------------------------------
Brief.- Initialize temperature sensor so that it is ready to receive commands.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef structure that. 
  contains the configuration I2C, GPIOS.
Param.- <void> None.
Return.- void None.
----------------------------------------------------------------*/
void MOD_TEMP_Init(TEMP_HandleTypeDef *htemp)
{

   MOD_TEMP_MspInit(htemp);
  
}


/*/*---------------------------------------------------------------
Brief.- Initialize the ALARM PIN MSP.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- void None.
----------------------------------------------------------------*/
__weak void MOD_TEMP_MspInit(TEMP_HandleTypeDef *htemp)
{

  UNUSED(htemp);

}


/*/*---------------------------------------------------------------
Brief.- Read the temperature sensor.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- uint16_t  Temperature reading.
----------------------------------------------------------------*/
uint16_t MOD_TEMP_Read(TEMP_HandleTypeDef *htemp)
{

   uint16_t Read_Temp = 0;
   Read_Temp = MOD_TEMP_READ_16BIT(htemp, TEMP_P_REG_TEMPERATURE_AMBIE);

   /*  Temperature Conversion  °C*/
   Read_Temp &= 0x0FFF;  //Clear flag bits (TA<15:13>)
   Read_Temp /= 16;

      if (Read_Temp & 0x1000)  //Clear SIGN  determine the SIGN bit (bit 12)
      Read_Temp -= 256;

  
   return Read_Temp;

}


/*/*---------------------------------------------------------------
Brief.- Set a temperature threshold.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef. 
Param.- <lower>  lower alarm.
Param.- <upper>  top alarm.
Return.- None.
----------------------------------------------------------------*/
void MOD_TEMP_SetAlarms(TEMP_HandleTypeDef *htemp, uint16_t lower, uint16_t upper)
{
   
    uint8_t data_aux[4];
    data_aux[0] = TEMP_P_REG_CONFIG;
    data_aux[1] = 0x00;
    data_aux[2] = TEMP_REG_CONFIG_ALERTCTRL;  //TEMP_REG_CONFIG_ALERTCTRL 1 ENABLED
    HAL_I2C_Master_Transmit(htemp->I2cHandler, TEMP_ADDRESS_DIVICE, data_aux, 3, HAL_MAX_DELAY);
    

    MOD_TEMP_WRITE_16BIT(htemp, TEMP_P_REG_TEMPERATURE_UPPER, upper);
    MOD_TEMP_WRITE_16BIT(htemp, TEMP_P_REG_TEMPERATURE_LOWER, lower);
    MOD_TEMP_WRITE_16BIT(htemp, TEMP_P_REG_TEMPERATURE_CRIT, (upper + 5) );

}


/*/*---------------------------------------------------------------
Brief.- Disable the set alarm window.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- None.
----------------------------------------------------------------*/
void MOD_TEMP_DisableAlarm(TEMP_HandleTypeDef *htemp)
{
  
    uint8_t data_aux[4];
    data_aux[0] = TEMP_P_REG_CONFIG;
    data_aux[1] = 0x0000;
    data_aux[2] = 0x0000; //TEMP_REG_CONFIG_ALERTCTRL 0 DISABLED
    HAL_I2C_Master_Transmit(htemp->I2cHandler, TEMP_ADDRESS_DIVICE, data_aux, 3, HAL_MAX_DELAY);
   
    MOD_TEMP_WRITE_16BIT(htemp, TEMP_P_REG_TEMPERATURE_UPPER, 0x0000);
    MOD_TEMP_WRITE_16BIT(htemp, TEMP_P_REG_TEMPERATURE_LOWER, 0x0000);
    MOD_TEMP_WRITE_16BIT(htemp, TEMP_P_REG_TEMPERATURE_CRIT,  0x0000);
   
}


/*/*---------------------------------------------------------------
Brief.- Disable the set alarm window.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- None.
----------------------------------------------------------------*/
uint16_t MOD_TEMP_GetAlarm_Upper(TEMP_HandleTypeDef *htemp)
{
   
    uint16_t Register_Value = 0;

    Register_Value = MOD_TEMP_READ_16BIT(htemp, TEMP_P_REG_TEMPERATURE_UPPER);

       /*  Temperature Conversion  °C*/
    Register_Value &= 0x0FFF;  //Clear flag bits (TA<15:13>)
    Register_Value /= 16;

      if (Register_Value & 0x1000)  //Clear SIGN  determine the SIGN bit (bit 12)
      Register_Value -= 256;

  
    return Register_Value;   

}


/*/*---------------------------------------------------------------
Brief.- Disable the set alarm window.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- None.
----------------------------------------------------------------*/
uint16_t MOD_TEMP_GetAlarm_Lower(TEMP_HandleTypeDef *htemp)
{
   
    uint16_t Register_Value = 0;

    Register_Value = MOD_TEMP_READ_16BIT(htemp, TEMP_P_REG_TEMPERATURE_LOWER);

       /*  Temperature Conversion  °C*/
    Register_Value &= 0x0FFF;  //Clear flag bits (TA<15:13>)
    Register_Value /= 16;

    if (Register_Value & 0x1000)  //Clear SIGN  determine the SIGN bit (bit 12)
     Register_Value -= 256;

  
    return Register_Value;
}


/*/*---------------------------------------------------------------
Brief.- Disable the set alarm window.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- None.
----------------------------------------------------------------*/
static uint16_t MOD_TEMP_READ_16BIT(TEMP_HandleTypeDef *htemp, uint8_t reg)
{

   uint8_t pData[2];
   uint16_t ReadData = 0;
   HAL_I2C_Mem_Read(htemp->I2cHandler, TEMP_ADDRESS_DIVICE, reg, 0x00000001U, pData, 2, HAL_MAX_DELAY);
   ReadData = (pData[0] << 8);
   ReadData |= pData[1];
   
   return ReadData;

}


/*/*---------------------------------------------------------------
Brief.- Disable the set alarm window.
Param.- <htemp>  pointer to a TEMP_HandleTypeDef.
Param.- <void> None.
Return.- None.
----------------------------------------------------------------*/
static void MOD_TEMP_WRITE_16BIT(TEMP_HandleTypeDef *htemp, uint8_t reg, uint16_t data)
{
   
   uint8_t data_aux[2];
   data_aux[0] = data >> 4; //MSB 
   data_aux[1] = data << 4; //LSB
   HAL_I2C_Mem_Write(htemp->I2cHandler, TEMP_ADDRESS_DIVICE, reg, 0x00000001U, data_aux, 2, HAL_MAX_DELAY);

} 