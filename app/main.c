/**------------------------------------------------------------------------------------------------
Brief.- CASIOTRON 5000 
-------------------------------------------------------------------------------------------------*/

#include "app_bsp.h"
#include "app_serial.h"
#include "app_clock.h"

/* Includes ------------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static uint32_t tickstart_led2 = 0;
static uint32_t tickstart_wwdg = 0;
static WWDG_HandleTypeDef WWDG_Handler;
SPI_HandleTypeDef SPI_Handler;
I2C_HandleTypeDef I2C_Handle;

/* Private function prototypes -----------------------------------------------*/

static void heart_init(void);
static void heart_beat(void);
static void dog_init(void);
static void peth_the_dog(void);
static void spi_init(void);
static void i2c_init(void);

/*----------------------------------------------------------------------------*/

int main(void)
{
  HAL_Init();
  //Other initializations

  heart_init(); 
  spi_init();
  i2c_init();
  serial_init();
  clock_init();
  dog_init();
  

  while (1)
  {
    serial_task();
    clock_task();
    heart_beat();
    peth_the_dog();
  }

  return 0u;
}


/*/**---------------------------------------------------------------
Brief.- Initializa LD2 (GPIOA GPIO_PIN_5)
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void heart_init(void)
{
   GPIO_InitTypeDef GPIO_InitStruct;

  __HAL_RCC_GPIOA_CLK_ENABLE();
   
  GPIO_InitStruct.Pin   = GPIO_PIN_5 | GPIO_PIN_4;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  tickstart_led2 = HAL_GetTick();

}


/*/**---------------------------------------------------------------
Brief.- Toggle LED2 300 ms
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void heart_beat(void)
{
   if( (HAL_GetTick() - tickstart_led2) >= 300 )
   {
     tickstart_led2 = HAL_GetTick();
     HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_4);
   } 

}


/*/**---------------------------------------------------------------
Brief.-  Initializa WWDG peripheral
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void dog_init(void)
{

  if ( __HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET )
  {
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);

  }

  __HAL_RCC_CLEAR_RESET_FLAGS();

  WWDG_Handler.Instance       = WWDG;
  WWDG_Handler.Init.Prescaler = WWDG_PRESCALER_8;
  WWDG_Handler.Init.Window    = 79;  //77      //83 //Min: 0x40- 64 , Max:0x7F- 127
  WWDG_Handler.Init.Counter   = 121;  //199 ->30ms
  WWDG_Handler.Init.EWIMode   = WWDG_EWI_DISABLE;

  HAL_WWDG_Init(&WWDG_Handler);

  tickstart_wwdg = HAL_GetTick();
}


/*/**---------------------------------------------------------------
Brief.- Refresh WWDG (29) ms
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void peth_the_dog(void)
{
   if( (HAL_GetTick() - tickstart_wwdg) >= 30 ) 
   {
     tickstart_wwdg = HAL_GetTick();

     HAL_WWDG_Refresh(&WWDG_Handler);
   } 
}


/*/**---------------------------------------------------------------
Brief.- Initializa peripheral SPI 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void spi_init(void)
{

   /* SPI Initialization */
    SPI_Handler.Instance               = SPI2;
    SPI_Handler.Init.Mode              = SPI_MODE_MASTER;
    SPI_Handler.Init.Direction         = SPI_DIRECTION_2LINES; //BIDIOE = 0  2-line unidirectional lcd_data mode selected
    SPI_Handler.Init.DataSize          = SPI_DATASIZE_8BIT;
    SPI_Handler.Init.CLKPolarity       = SPI_POLARITY_LOW;
    SPI_Handler.Init.CLKPhase          = SPI_PHASE_2EDGE;
    SPI_Handler.Init.NSS               = SPI_NSS_SOFT;
    SPI_Handler.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16; // 3-MHZ
    SPI_Handler.Init.FirstBit          = SPI_FIRSTBIT_MSB;  //BD7
    SPI_Handler.Init.TIMode            = SPI_TIMODE_DISABLE; //TI protocol in master mode Disable
    SPI_Handler.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE; 
    SPI_Handler.Init.CRCPolynomial     = 7;
    SPI_Handler.Init.CRCLength         = SPI_CRC_LENGTH_8BIT;
    SPI_Handler.Init.NSSPMode          = SPI_NSS_PULSE_DISABLE;

    HAL_SPI_Init(&SPI_Handler);

}


/*/**---------------------------------------------------------------
Brief.- Initializa peripheral I2C 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void i2c_init(void)
{
     /* I2C Initialization */
   I2C_Handle.Instance              = I2C1;
   I2C_Handle.Init.Timing           = 0x10815E89;
   I2C_Handle.Init.OwnAddress1      = 0;
   I2C_Handle.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
   I2C_Handle.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
   I2C_Handle.Init.OwnAddress2      = 0;
   I2C_Handle.Init.OwnAddress2Masks = I2C_OA2_NOMASK;  
   I2C_Handle.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
   I2C_Handle.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;

   HAL_I2C_Init(&I2C_Handle);

}