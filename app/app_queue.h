/**------------------------------------------------------------------------------------------------
Brief.- app_queue.h
Waiting queue to communicate tasks
-------------------------------------------------------------------------------------------------*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _APP_QUEUE_H_
#define _APP_QUEUE_H_

/* Includes ------------------------------------------------------------------*/

#include "app_bsp.h"

/* Exported typedef -----------------------------------------------------------*/

typedef struct
{
    void  	  *Buffer;     /*!< Buffer declarade array void * >*/

    uint32_t  Elements;    /*!< Elements the array    >*/

    uint8_t   Size;        /*!< Size the type the elements  >*/

    uint32_t  Head;       /*!< Variable Head WRITE in buffer >*/

    uint32_t  Tail;       /*!< Variable Tail READ in buffer >*/

    uint8_t	  Empty;      /*!< Variable buffer is empty >*/

    uint8_t	  Full;       /*!< Variable buffer is Full >*/

}QUEUE_HandleTypeDef;

/* Exported define ------------------------------------------------------------*/
/* Exported macro -------------------------------------------------------------*/
/* Exported variables ---------------------------------------------------------*/
/* Exported functions prototypes ----------------------------------------------*/

void HIL_QUEUE_Init(QUEUE_HandleTypeDef *hqueue);
uint8_t HIL_QUEUE_Write(QUEUE_HandleTypeDef *hqueue, void *data);
uint8_t HIL_QUEUE_Read(QUEUE_HandleTypeDef *hqueue, void *data);
uint8_t HIL_QUEUE_IsEmpty(QUEUE_HandleTypeDef *hqueue);

/*-----------------------------------------------------------------------------*/

#endif