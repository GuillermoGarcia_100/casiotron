/**------------------------------------------------------------------------------------------------
Brief.- CASIOTRON 5000 
State maquine for clock with RTC peripheral
-------------------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

#include "app_clock.h"
#include "app_lcd.h"
#include "app_queue.h"
#include "app_temp.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/*   Machine States  1  */
#define STATE_0_INITIAL                        0
#define STATE_1_LCD_TIME_HOURS                 1
#define STATE_2_LCD_TIME_MINUTES               2
#define STATE_3_LCD_TIME_SECONDS               3
#define STATE_4_LCD_DATE_MONTH                 4
#define STATE_5_LCD_DATE_DAY                   5
#define STATE_6_LCD_DATE_YEAR                  6
#define STATE_7_LCD_ALARM                      7
#define STATE_8_LCD_ALARM_CONFIG               8
#define STATE_9_LCD_NO_ALARM_CONFIG            9
#define STATE_10_CONFIG_TIME_DATE_ALARM_TEMP   10
#define STATE_11_READ_BOTTOM                   11
#define STATE_12_LCD_CLEAR                     12
#define STATE_13_READ_TEMPERATURE              13
#define STATE_14_LCD_PRE_ALARM                 14

 /*          GetTime and GetDate          */
#define GET_TIME_HOURS                         0
#define GET_TIME_MINUTES                       1
#define GET_TIME_SECONDS                       2
#define GET_DATE_WEEKDAY                       3
#define GET_DATE_MONTH                         4
#define GET_DATE_DATE                          5
#define GET_DATE_YEAR                          6

/* Private function prototypes -----------------------------------------------*/

static void Configure_Time(void);
static void Configure_Date(void);
static void Configure_Alarm(void);
static void Configure_Temp(void);

/*  funtics Machine States   */
static void State_Initial(void);
static void Lcd_Time_Hours(void);
static void Lcd_Time_Minutes(void);
static void Lcd_Time_Seconds(void);
static void Lcd_Date_Month(void);
static void Lcd_Date_Day(void);
static void Lcd_Date_Year(void);
static void Lcd_Alarm(void);
static void Lcd_Alarm_Config(void);
static void Lcd_No_Alarm_Config(void);
static void Config_Time_Date_Alarm_Temp(void);
static void Read_Bottom(void);
static void Lcd_Clear(void);
static void Lcd_Temp_Read(void);
static void Lcd_Pre_Alarm(void);

static uint8_t GetTime_RTC(uint8_t param_return);
static uint8_t GetDate_RTC(uint8_t param_return);

static void Int_to_Str(uint32_t num, uint8_t *str);
static uint8_t Date_Weekday(const uint8_t Day, const uint8_t Moth, const uint8_t Year);
static void Lcd_Init(void);
static void MCP9808_Init(void);

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static uint32_t tickstart_Bottom  = 0; 
static uint32_t tickstart_Temp    = 0;

static uint8_t second_RTC  = 0;
static uint8_t minutes_RTC = 0;
static uint8_t hours_RTC   = 0;

static uint8_t month_RTC   = 0;
static uint8_t day_RTC     = 0;
static uint8_t year_RTC    = 0;


/*  Flag variables                  */
static uint8_t state               = STATE_0_INITIAL;
static uint8_t Bottom_SET          = SET;
static uint8_t LCD_Claer           = RESET;
static uint8_t Alarm_SET           = RESET;
static uint8_t Alarm_Temp_SET      = RESET;
static uint8_t Pre_Aalrm_SET       = RESET;

/*  Flag variables from Alarms      */
static __IO ITStatus stat_IT_ALARM = RESET;
static __IO ITStatus stat_Temp_ALARM = RESET;

/*    Typedef Handles              */
static LCD_HandleTypeDef   LCD_Handler;
static SERIAL_MsgTypeDef   MsgToRead;
static TEMP_HandleTypeDef  Temp_Handle;

extern SPI_HandleTypeDef   SPI_Handler;
extern QUEUE_HandleTypeDef Queue2_Handler;
extern I2C_HandleTypeDef   I2C_Handle;

/*    Const data                   */
static const uint8_t *Moth[] = {(const uint8_t*)"ENE,", (const uint8_t*)"FEB,", (const uint8_t*)"MAR,", 
(const uint8_t*)"ABR,", (const uint8_t*)"MAY,", (const uint8_t*)"JUN,", (const uint8_t*)"JUL,", 
(const uint8_t*)"AGO,", (const uint8_t*)"SEP,", (const uint8_t*)"OCT,", (const uint8_t*)"NOV,", 
(const uint8_t*)"DIC," };

static const uint8_t *Weekday[] = {(const uint8_t*)"LU", (const uint8_t*)"MA", (const uint8_t*)"MI",
(const uint8_t*)"JU", (const uint8_t*)"VI", (const uint8_t*)"SA", (const uint8_t*)"DO" };

/*  Pointer at Fuctions    Maquine clock_task()   */
static void(*Ptr_fun_Maquine_clock[])(void) = { State_Initial, Lcd_Time_Hours, 
Lcd_Time_Minutes, Lcd_Time_Seconds, Lcd_Date_Month, Lcd_Date_Day, Lcd_Date_Year,
Lcd_Alarm, Lcd_Alarm_Config, Lcd_No_Alarm_Config, Config_Time_Date_Alarm_Temp, Read_Bottom, Lcd_Clear,
Lcd_Temp_Read, Lcd_Pre_Alarm };

/*  Pointer at Fuctions state  STATE_6_F_Config_time_date_alarm()  */
static void (*Ptr_fun_Configure[])(void) = { Configure_Time, Configure_Date, Configure_Alarm, Configure_Temp};
/* ---------------------------------------------------------------------------*/

/*/**---------------------------------------------------------------
Brief.- Function Initialization the RTC peripheral and LCD Init
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void clock_init(void)
{

   Rtc_Init.Instance = RTC;

   Rtc_Init.Init.HourFormat      = RTC_HOURFORMAT_24;
   Rtc_Init.Init.AsynchPrediv    = 127; //Asynchronous 7-bit prescaler 127-Default
   Rtc_Init.Init.SynchPrediv     = 255;  //Synchronous 15-bit prescaler  255-Default
   Rtc_Init.Init.OutPut          = RTC_OUTPUT_DISABLE;
   Rtc_Init.Init.OutPutPolarity  = RTC_OUTPUT_POLARITY_HIGH;
   Rtc_Init.Init.OutPutType      = RTC_OUTPUT_TYPE_OPENDRAIN;

   HAL_RTC_Init(&Rtc_Init);
   
   Lcd_Init();
   MCP9808_Init();

   tickstart_Bottom    = HAL_GetTick();
   tickstart_Temp      = HAL_GetTick();

   /*   Initializa value in LCD */
   hours_RTC    = GetTime_RTC(GET_TIME_HOURS);
   minutes_RTC  = GetTime_RTC(GET_TIME_MINUTES);
   second_RTC   = GetTime_RTC(GET_TIME_SECONDS);

   month_RTC    = GetDate_RTC(GET_DATE_MONTH);
   day_RTC      = GetDate_RTC(GET_DATE_DATE);
   year_RTC     = GetDate_RTC(GET_DATE_YEAR);
  
   Lcd_Time_Hours();
   Lcd_Time_Minutes();
   Lcd_Time_Seconds();

   Lcd_Date_Month();
   Lcd_Date_Day();
   Lcd_Date_Year();

}


/*/**---------------------------------------------------------------
Brief.- Clock task, clock machine, RTC peripheral 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void clock_task(void)
{

   Ptr_fun_Maquine_clock[state]();

}


/*/**---------------------------------------------------------------
Brief.- Machine, STATE_0_F_Initial the clock_task()
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void State_Initial(void)
{
   /*                   TIME                        */
   if ( (hours_RTC - GetTime_RTC(GET_TIME_HOURS)) != 0)
   {
       hours_RTC = GetTime_RTC(GET_TIME_HOURS);
       state = STATE_1_LCD_TIME_HOURS;
   }
   else if ( (minutes_RTC - GetTime_RTC(GET_TIME_MINUTES)) != 0)
   { 
       minutes_RTC = GetTime_RTC(GET_TIME_MINUTES);
       state = STATE_2_LCD_TIME_MINUTES;
   }
   else if ( (second_RTC - GetTime_RTC(GET_TIME_SECONDS)) != 0 && (Bottom_SET == SET) )
   {
       second_RTC = GetTime_RTC(GET_TIME_SECONDS);
       state = STATE_3_LCD_TIME_SECONDS;
   }
   /*                  DATE                               */
   else if ( (month_RTC - GetDate_RTC(GET_DATE_MONTH)) != 0)
   {
       month_RTC = GetDate_RTC(GET_DATE_MONTH);
       state = STATE_4_LCD_DATE_MONTH;
   }
   else if ( (day_RTC - GetDate_RTC(GET_DATE_DATE)) != 0)
   {
       day_RTC = GetDate_RTC(GET_DATE_DATE);
       state = STATE_5_LCD_DATE_DAY;
   }
   else if ( (year_RTC - GetDate_RTC(GET_DATE_YEAR)) != 0)
   {
       year_RTC = GetDate_RTC(GET_DATE_YEAR);
       state = STATE_6_LCD_DATE_YEAR;
   }
   else if ( (HAL_GetTick() - tickstart_Bottom) >= 50 )
   {
       tickstart_Bottom = HAL_GetTick();
       state = STATE_11_READ_BOTTOM;
   }
   else if ( (HAL_GetTick() - tickstart_Temp) >= 251 && (Bottom_SET == SET) )
   {
       tickstart_Temp = HAL_GetTick();
       state = STATE_13_READ_TEMPERATURE;
   }  
   else if ( HIL_QUEUE_Read(&Queue2_Handler, &MsgToRead) == 1)
   {
       state = STATE_10_CONFIG_TIME_DATE_ALARM_TEMP;
   }

}


/*/**---------------------------------------------------------------
Brief.- Machine Date display function on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Time_Hours(void)
{
   
   uint8_t buffer_Time_Hours[4];

   Int_to_Str(hours_RTC, buffer_Time_Hours);
  
   MOD_LCD_SetCursor(&LCD_Handler, 1, 0);
   MOD_LCD_String(&LCD_Handler, buffer_Time_Hours);
   MOD_LCD_Data(&LCD_Handler, ':');

   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine Date display function on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Time_Minutes(void)
{

   uint8_t buffer_Time_Minutes[4];

   Int_to_Str(minutes_RTC, buffer_Time_Minutes);
  
   MOD_LCD_SetCursor(&LCD_Handler, 1, 3);
   MOD_LCD_String(&LCD_Handler, buffer_Time_Minutes);
   MOD_LCD_Data(&LCD_Handler, ':');

   if (stat_IT_ALARM == SET)
   {
     stat_IT_ALARM   = RESET;
     Alarm_SET       = RESET;
     Pre_Aalrm_SET   = RESET;
     LCD_Claer       = SET;
     HAL_RTC_DeactivateAlarm(&Rtc_Init, RTC_ALARM_A);
   }

   if (stat_Temp_ALARM == SET)
   {
     stat_Temp_ALARM = RESET;
     Alarm_Temp_SET  = RESET;
     Pre_Aalrm_SET   = RESET;
     LCD_Claer       = SET;
     MOD_TEMP_DisableAlarm(&Temp_Handle);
     //Pin_Alert_SET   = SET;
   }

   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine displays alarmed display on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Time_Seconds(void)
{

   uint8_t buffer_Time_Seconds[4];
   
   if (stat_IT_ALARM == RESET && stat_Temp_ALARM == RESET)
   {
     Int_to_Str(second_RTC, buffer_Time_Seconds);
     MOD_LCD_SetCursor(&LCD_Handler, 1, 6);
     MOD_LCD_String(&LCD_Handler, buffer_Time_Seconds);
   }
   
   if (Alarm_SET == SET && stat_IT_ALARM == RESET)
   {
     MOD_LCD_SetCursor(&LCD_Handler, 1, 14);
     MOD_LCD_Data(&LCD_Handler, 'A');
   }

   if (Alarm_Temp_SET == SET && stat_Temp_ALARM == RESET)
   {
     MOD_LCD_SetCursor(&LCD_Handler, 1, 15);
     MOD_LCD_Data(&LCD_Handler, 'T');
   }

   if ( (stat_IT_ALARM == SET || stat_Temp_ALARM == SET) && Pre_Aalrm_SET == RESET)
   {
     state = STATE_14_LCD_PRE_ALARM;
   }
   else if ( (stat_IT_ALARM == SET || stat_Temp_ALARM == SET) && Pre_Aalrm_SET == SET)
   {
     state = STATE_7_LCD_ALARM;
   }
   else
   {
     state = STATE_0_INITIAL;
   }
   
  
}


/*/**---------------------------------------------------------------
Brief.- Machine Date display function on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Date_Month(void)
{
  
   MOD_LCD_SetCursor(&LCD_Handler, 0, 1);
   MOD_LCD_String(&LCD_Handler, (uint8_t *)Moth[(month_RTC-1)] );
   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine Date display function on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Date_Day(void)
{
   
   uint8_t buffer_Date_Day[4];
   Int_to_Str(day_RTC, buffer_Date_Day);

   MOD_LCD_SetCursor(&LCD_Handler, 0, 5);
   MOD_LCD_String(&LCD_Handler, buffer_Date_Day);

   MOD_LCD_SetCursor(&LCD_Handler, 0, 13);
   MOD_LCD_String(&LCD_Handler, (uint8_t *)Weekday[ (GetDate_RTC(GET_DATE_WEEKDAY)-1) ] );

   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine Date display function on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Date_Year(void)
{

   uint8_t buffer_Date_Year[4];
   Int_to_Str(year_RTC, buffer_Date_Year);
   MOD_LCD_SetCursor(&LCD_Handler, 0, 8);
   MOD_LCD_String(&LCD_Handler, (uint8_t *)"20");
   MOD_LCD_String(&LCD_Handler, buffer_Date_Year);
  
   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine displays alarmed display on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Alarm(void)
{
 
  static uint8_t state_Display_ALARM = 0;

    switch (state_Display_ALARM)
    {
      case 0:
       MOD_LCD_SetCursor(&LCD_Handler, 1, 0);
       MOD_LCD_String(&LCD_Handler, (uint8_t *)"***");

       MOD_LCD_SetCursor(&LCD_Handler, 1, 13);
       MOD_LCD_String(&LCD_Handler, (uint8_t *)"***");
       state_Display_ALARM = 1;
       break;
 
      case 1:
       MOD_LCD_SetCursor(&LCD_Handler, 1, 0);
       MOD_LCD_String(&LCD_Handler, (uint8_t *)"   ");

       MOD_LCD_SetCursor(&LCD_Handler, 1, 13);
       MOD_LCD_String(&LCD_Handler, (uint8_t *)"   ");
       state_Display_ALARM = 0;
       break;


      default:
       state_Display_ALARM = 0;
       break;
     }

  state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine displays alarm settings on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Alarm_Config(void)
{

   uint8_t buffer_Alarm_Hours[4];
   uint8_t buffer_Alarm_Minutes[4];
   uint8_t buffer_Alarm_Upper[4];
   uint8_t buffer_Alarm_Lower[4];

   RTC_AlarmTypeDef RTC_GetAlarm_Struct;
   
   MOD_LCD_SetCursor(&LCD_Handler, 1, 0);
   MOD_LCD_String(&LCD_Handler, (uint8_t *)" A ");

   if (Alarm_SET == SET)
   {   
     HAL_RTC_GetAlarm(&Rtc_Init, &RTC_GetAlarm_Struct, RTC_ALARM_A, RTC_FORMAT_BIN);

     Int_to_Str(RTC_GetAlarm_Struct.AlarmTime.Hours, buffer_Alarm_Hours);
     Int_to_Str(RTC_GetAlarm_Struct.AlarmTime.Minutes, buffer_Alarm_Minutes);
  
     MOD_LCD_String(&LCD_Handler, buffer_Alarm_Hours);
     MOD_LCD_Data(&LCD_Handler, ':');

     MOD_LCD_String(&LCD_Handler, buffer_Alarm_Minutes);


     if (Alarm_Temp_SET == RESET)
     {
       MOD_LCD_String(&LCD_Handler, (uint8_t *)"        ");
     }

   }

   if (Alarm_Temp_SET == SET)
   {

     if(Alarm_SET == RESET)
     {
       MOD_LCD_SetCursor(&LCD_Handler, 1, 3);
       MOD_LCD_String(&LCD_Handler, (uint8_t *)"      ");
     }


     Int_to_Str((uint32_t)MOD_TEMP_GetAlarm_Lower(&Temp_Handle), buffer_Alarm_Lower);
     Int_to_Str((uint32_t)MOD_TEMP_GetAlarm_Upper(&Temp_Handle), buffer_Alarm_Upper);
    
     MOD_LCD_SetCursor(&LCD_Handler, 1, 9);
     MOD_LCD_String(&LCD_Handler, buffer_Alarm_Lower);
     MOD_LCD_Data(&LCD_Handler, '-');
     MOD_LCD_String(&LCD_Handler, buffer_Alarm_Upper);
     MOD_LCD_String(&LCD_Handler, (uint8_t *)"C ");
   }

   LCD_Claer = SET;
   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Machine shows no alarm set on LCD 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_No_Alarm_Config(void)
{

  MOD_LCD_SetCursor(&LCD_Handler, 1, 0);
  MOD_LCD_String(&LCD_Handler, (uint8_t *)"NO ALARMS CONFIG"); 

  LCD_Claer = SET;
  state = STATE_0_INITIAL;
   
}


/*/**---------------------------------------------------------------
Brief.- Pointer to function that will configure peripheral RTC parameters 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Config_Time_Date_Alarm_Temp(void)
{

  Ptr_fun_Configure[MsgToRead.msg]();
  state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Function to read button 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Read_Bottom(void)
{
   if (Alarm_Temp_SET == SET)
   {
     if ( HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_15) == GPIO_PIN_RESET )
     {
       stat_Temp_ALARM = SET;
     }

   }

   if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == GPIO_PIN_RESET)
   {
       Bottom_SET = RESET;

       if (stat_Temp_ALARM == SET)
       {
         stat_Temp_ALARM = RESET;
         Alarm_Temp_SET  = RESET;
         Pre_Aalrm_SET   = RESET;
         LCD_Claer       = SET;
         MOD_TEMP_DisableAlarm(&Temp_Handle);

         state = STATE_0_INITIAL;
       }
       else if (stat_IT_ALARM == SET)
       {
           stat_IT_ALARM   = RESET;
           Alarm_SET       = RESET;
           Pre_Aalrm_SET   = RESET;
           LCD_Claer       = SET;
           HAL_RTC_DeactivateAlarm(&Rtc_Init, RTC_ALARM_A);

           state = STATE_0_INITIAL;
       }
       else if (Alarm_SET == SET || Alarm_Temp_SET)
       {
           state = STATE_8_LCD_ALARM_CONFIG;
       }
       else
       {
           state = STATE_9_LCD_NO_ALARM_CONFIG;
       }
  
   }
   else
   {
       Bottom_SET = SET;

       if(LCD_Claer == SET)
       {
           state = STATE_12_LCD_CLEAR;
       }
       else
       {
           state = STATE_0_INITIAL;
       }  
      
   }

}


/*/**---------------------------------------------------------------
Brief.- Function to read button 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Clear(void)
{

   Lcd_Time_Hours();
   Lcd_Time_Minutes();


   MOD_LCD_SetCursor(&LCD_Handler, 1, 8);
   MOD_LCD_String(&LCD_Handler, (uint8_t *)"    ");

   MOD_LCD_SetCursor(&LCD_Handler, 1, 12);
   MOD_LCD_String(&LCD_Handler, (uint8_t *)"    ");

   LCD_Claer = RESET;

   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Function to read button 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Temp_Read(void)
{
  
   uint16_t ReadTemp = 0;
   uint8_t  buffer_Temperature[4];

   ReadTemp = MOD_TEMP_Read(&Temp_Handle);

   Int_to_Str( (uint32_t)ReadTemp, buffer_Temperature);
   
   if (Pre_Aalrm_SET == RESET)
   {
     MOD_LCD_SetCursor(&LCD_Handler, 1, 9);
     MOD_LCD_String(&LCD_Handler, buffer_Temperature);
     MOD_LCD_Data(&LCD_Handler, 'C');

     state = STATE_0_INITIAL;
   }
   else
   {
     MOD_LCD_SetCursor(&LCD_Handler, 1, 10);
     MOD_LCD_String(&LCD_Handler, buffer_Temperature);
     MOD_LCD_Data(&LCD_Handler, 'C');

     state = STATE_7_LCD_ALARM;
   }


}


/*/**---------------------------------------------------------------
Brief.- Function to read button 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Pre_Alarm(void)
{ 
  
   uint8_t buffer_Alarm_Hours[4];
   uint8_t buffer_Alarm_Minutes[4];

   Int_to_Str(hours_RTC, buffer_Alarm_Hours);
   Int_to_Str(minutes_RTC, buffer_Alarm_Minutes);

   MOD_LCD_SetCursor(&LCD_Handler, 1, 3);
   MOD_LCD_Data(&LCD_Handler, ' ');
   MOD_LCD_String(&LCD_Handler, buffer_Alarm_Hours);
   MOD_LCD_Data(&LCD_Handler, ':');
   MOD_LCD_String(&LCD_Handler, buffer_Alarm_Minutes);
   MOD_LCD_Data(&LCD_Handler, ' ');

   Pre_Aalrm_SET = SET;

   state = STATE_13_READ_TEMPERATURE;

}


/*/**---------------------------------------------------------------
Brief.- Function Setting with (HAL_RTC_SetTime) TIME in RTC peripheral 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Configure_Time(void)
{

   RTC_TimeTypeDef RTC_Time_InitStruc;
   //Configure Time
   RTC_Time_InitStruc.Hours          = MsgToRead.param1;
   RTC_Time_InitStruc.Minutes        = MsgToRead.param2;
   RTC_Time_InitStruc.Seconds        = MsgToRead.param3;
   (MsgToRead.param1 <= 12)  ? (RTC_Time_InitStruc.TimeFormat = RTC_HOURFORMAT12_AM) : 
   (RTC_Time_InitStruc.TimeFormat    = RTC_HOURFORMAT12_PM);
   RTC_Time_InitStruc.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
   RTC_Time_InitStruc.StoreOperation = RTC_STOREOPERATION_RESET;
  
   HAL_RTC_SetTime(&Rtc_Init, &RTC_Time_InitStruc, RTC_FORMAT_BIN);

}


/*/**---------------------------------------------------------------
Brief.- Function Setting with (HAL_RTC_SetDate) DATE in RTC peripheral 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Configure_Date(void)
{
  
   RTC_DateTypeDef RTC_Date_InitStruc;
   //Configure Date
   RTC_Date_InitStruc.Year    = MsgToRead.param3;
   RTC_Date_InitStruc.Month   = MsgToRead.param2;
   RTC_Date_InitStruc.Date    = MsgToRead.param1;
   RTC_Date_InitStruc.WeekDay = Date_Weekday(MsgToRead.param1, MsgToRead.param2, MsgToRead.param3);

   HAL_RTC_SetDate(&Rtc_Init, &RTC_Date_InitStruc, RTC_FORMAT_BIN);

}


/*/**---------------------------------------------------------------
Brief.- Function Setting with (HAL_RTC_SetAlarm) ALRM in RTC peripheral 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Configure_Alarm(void)
{

   RTC_AlarmTypeDef RTC_Alarm_InitStruc;

   //Configure Alarm
   RTC_Alarm_InitStruc.Alarm                   = RTC_ALARM_A;
   RTC_Alarm_InitStruc.AlarmDateWeekDay        = RTC_WEEKDAY_MONDAY;
   RTC_Alarm_InitStruc.AlarmDateWeekDaySel     = RTC_ALARMDATEWEEKDAYSEL_DATE;
   RTC_Alarm_InitStruc.AlarmMask               = RTC_ALARMMASK_DATEWEEKDAY;
   RTC_Alarm_InitStruc.AlarmSubSecondMask      = RTC_ALARMSUBSECONDMASK_NONE;
   RTC_Alarm_InitStruc.AlarmTime.TimeFormat    = RTC_HOURFORMAT_24;
   RTC_Alarm_InitStruc.AlarmTime.Hours         = MsgToRead.param1;
   RTC_Alarm_InitStruc.AlarmTime.Minutes       = MsgToRead.param2;
   RTC_Alarm_InitStruc.AlarmTime.Seconds       = 0x00;
   RTC_Alarm_InitStruc.AlarmTime.SubSeconds    = 0x00;

   HAL_RTC_SetAlarm_IT(&Rtc_Init, &RTC_Alarm_InitStruc, RTC_FORMAT_BIN);

   Alarm_SET = SET;

}


/*/**---------------------------------------------------------------
Brief.- Function Configure Temperature in MCP9808 
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Configure_Temp(void)
{

   MOD_TEMP_SetAlarms(&Temp_Handle, (uint16_t)MsgToRead.param1, (uint16_t)MsgToRead.param2);
   
   Alarm_Temp_SET = SET;


}


/*/**---------------------------------------------------------------
Brief.- Function Initializa LCD  
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void Lcd_Init(void)
{

   LCD_Handler.SpiHandler  = &SPI_Handler;
   LCD_Handler.RstPort     = GPIOB;
   LCD_Handler.RstPin      = GPIO_PIN_4;
   LCD_Handler.RsPort      = GPIOB;
   LCD_Handler.RsPin       = GPIO_PIN_3;
   LCD_Handler.CsPort      = GPIOB;
   LCD_Handler.CsPin       = GPIO_PIN_12;
     
   MOD_LCD_Init(&LCD_Handler);

}


/*/**---------------------------------------------------------------
Brief.- Function Initializa LCD  
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
static void MCP9808_Init(void)
{
    
   Temp_Handle.I2cHandler   = &I2C_Handle;
   Temp_Handle.AlertPort    = GPIOA;
   Temp_Handle.AlertPin     = GPIO_PIN_15;
  
   MOD_TEMP_Init(&Temp_Handle);

}


/*/**---------------------------------------------------------------
Brief.- Function convert int at str data 
Param.- <num> Number to convert to string
Param.- <str> String pointer where it saves the value 
Return.- void None
----------------------------------------------------------------*/
static void Int_to_Str(uint32_t num, uint8_t *str)
{

  uint8_t *string = str;
  uint8_t contenido;
  uint8_t Flag_plus_zero = 0;
    
  if(num <= 9)
  {
    Flag_plus_zero = 1;
  }

  do
  {

    *string++ = (num % 10) + '0';
    num  /= 10;
  } while (num);

  if (Flag_plus_zero)
  {
    *string = '0';
    string++;
  }

  *string = '\0';

  string--;
  while (string > str)
  {
    contenido = *string;
    *string-- = *str;
    *str++ = contenido;
  }

}


/*/**---------------------------------------------------------------
Brief.- Function Calculation Day the weekday 
Param.- <Day> Day is param1
Param.- <Moth> Moth is param2 
Param.- <Year> Year is param3
Return.- The day of the week
----------------------------------------------------------------*/
static uint8_t Date_Weekday(const uint8_t Day, const uint8_t Moth, const uint8_t Year)
{  

  uint8_t siglo = 6;
  uint8_t dia_anno = (Year / 4);
  uint8_t t_mes = 0;
  uint8_t dia_current;

  switch (Moth) 
     {
    case 1:
      t_mes=0;
      break;
    case 2:
      t_mes=3;
      break;
    case 3:
      t_mes=3;
      break;
    case 4:
      t_mes=6;
      break;
    case 5:
      t_mes=1;
      break;
    case 6:
      t_mes=4;
      break;
    case 7:
      t_mes=6;
      break;
    case 8:
      t_mes=2;
      break;
    case 9:
      t_mes=5;
      break;
    case 10:
      t_mes=0;
      break;
    case 11:
      t_mes=3;
      break;
    case 12:
      t_mes=5;
      break;
    default: 
      t_mes=t_mes;
    break;
 }

  dia_current = Day + t_mes + Year + dia_anno + siglo;
  dia_current = dia_current % 7;

  if(dia_current == 0)
  {
    return 7;
  }
  
   return dia_current;

}


/*/**---------------------------------------------------------------
Brief.- Function Callback the RTC peripheral ALARM_A Event
Param.- <hrtc> RTC typedef struct <unnamed> RTC_HandleTypeDef
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{

   stat_IT_ALARM = SET;
   state = STATE_0_INITIAL;

}


/*/**---------------------------------------------------------------
Brief.- Function Initializa LCD  
Param.- <void> None
Param.- <void> None
Return.- RTC time parameter
----------------------------------------------------------------*/
static uint8_t GetTime_RTC(uint8_t param_return)
{
   
  RTC_TimeTypeDef RTC_GetTime_Struct_L;
  RTC_DateTypeDef RTC_GetDate_Struct_L;

  HAL_RTC_GetTime(&Rtc_Init, &RTC_GetTime_Struct_L, RTC_FORMAT_BIN);
  HAL_RTC_GetDate(&Rtc_Init, &RTC_GetDate_Struct_L, RTC_FORMAT_BIN);

  switch (param_return)
  {
    case GET_TIME_HOURS:
     return  RTC_GetTime_Struct_L.Hours;
     break;
  
    case GET_TIME_MINUTES:
     return RTC_GetTime_Struct_L.Minutes;
     break;
    
    case GET_TIME_SECONDS:
     return RTC_GetTime_Struct_L.Seconds;
     break;

    default:
     return 0;
     break;
  }
  
}


/*/**---------------------------------------------------------------
Brief.- Function Initializa LCD  
Param.- <void> None
Param.- <void> None
Return.- RTC date parameter
----------------------------------------------------------------*/
static uint8_t GetDate_RTC(uint8_t param_return)
{
  RTC_TimeTypeDef RTC_GetTime_Struct_L;
  RTC_DateTypeDef RTC_GetDate_Struct_L;

  HAL_RTC_GetTime(&Rtc_Init, &RTC_GetTime_Struct_L, RTC_FORMAT_BIN);
  HAL_RTC_GetDate(&Rtc_Init, &RTC_GetDate_Struct_L, RTC_FORMAT_BIN);
  
  switch (param_return)
  {
    case GET_DATE_WEEKDAY:
     return RTC_GetDate_Struct_L.WeekDay;
     break;
  
    case GET_DATE_MONTH:
     return RTC_GetDate_Struct_L.Month;
     break;

    case GET_DATE_DATE:
     return RTC_GetDate_Struct_L.Date;
     break;

    case GET_DATE_YEAR:
     return RTC_GetDate_Struct_L.Year;
     break; 
  
    default:
     return 0;
     break;
  }

}