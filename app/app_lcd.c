/**------------------------------------------------------------------------------------------------
Brief.- app_lcd.c
2x16 alphanumeric LCD with spi interface
-------------------------------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

#include "app_lcd.h"
#include <string.h>
#include <stdint.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static uint8_t DDRAM_L0_Address[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};

static uint8_t DDRAM_L1_Address[16] = {0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F};

/* Private function prototypes -----------------------------------------------*/
/*----------------------------------------------------------------------------*/


/*/*---------------------------------------------------------------
Brief.- Initializes the LCD so that it is ready to receive data 
  to be displayed on its screen.
Param.- <hlcd>  pointer to a LCD_HandleTypeDef structure that 
  contains the configuration SPI, GPIOS.
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void MOD_LCD_Init(LCD_HandleTypeDef *hlcd)
{
    MOD_LCD_MspInit(hlcd);
                           /* CS = 1 */
    HAL_GPIO_WritePin(hlcd->CsPort, hlcd->CsPin, GPIO_PIN_SET);
                           
                          /* RST = 0 */
    HAL_GPIO_WritePin(hlcd->RstPort, hlcd->RstPin, GPIO_PIN_RESET);

    HAL_Delay(2);
                            /* RST = 1 */
    HAL_GPIO_WritePin(hlcd->RstPort, hlcd->RstPin, GPIO_PIN_SET);

    HAL_Delay(20);
    
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_WAKEUP);  
    HAL_Delay(2);  
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_WAKEUP);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_WAKEUP);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_FUNCTION_SET_P_8BIT_2LINE_MODE);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_INTERNAL_OSC_P_FREQUENCY_183_HZ);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_POWER_CONTROL_P_BOOSTER_ON);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_FOLLOWER_CONTROL_P_FOLLOWER_ON);  
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_CONTRAST_SET);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_DISPLAY_ON);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_ENTRY_MODE);
    MOD_LCD_Command(hlcd, LCD_INSTRUCTION_CLEAR_DISPLAY);
    HAL_Delay(1);

}


/*/*---------------------------------------------------------------
Brief.- This function should not be modified, when the callback is 
  needed, the MOD_LCD_MspInit can be implemented in the user file.
Param.- <hlcd>  pointer to a LCD_HandleTypeDef structure.
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
__weak void MOD_LCD_MspInit(LCD_HandleTypeDef *hlcd)
{
      /* Prevent unused argument(s) compilation warning */
     UNUSED(hlcd);
}


/*/*---------------------------------------------------------------
Brief.- Send an command the LCD it is ready to receive data.
Param.- <hlcd>  pointer to a LCD_HandleTypeDef structure that 
  contains the configuration SPI, GPIOS.
Param.- <cmd> Hexadecimal command, can be #define LCD_INSTRUCTION.
Return.- void None
----------------------------------------------------------------*/
void MOD_LCD_Command(LCD_HandleTypeDef *hlcd, uint8_t cmd)
{
                      /* instruction mode RS = 0 */
    HAL_GPIO_WritePin(hlcd->RsPort, hlcd->RsPin, GPIO_PIN_RESET);

                      /*chip select on CS = 0*/
    HAL_GPIO_WritePin(hlcd->CsPort, hlcd->CsPin, GPIO_PIN_RESET);
                      
                      /*Send SPI*/
    HAL_SPI_Transmit(hlcd->SpiHandler, &cmd, sizeof(cmd), 100);
                      
                      /*chip select off CS = 1 */
    HAL_GPIO_WritePin(hlcd->CsPort, hlcd->CsPin, GPIO_PIN_SET);
    
}


/*/*---------------------------------------------------------------
Brief.- Sends a data to the LCD.
Param.- <hlcd>  pointer to a LCD_HandleTypeDef structure that 
  contains the configuration SPI, GPIOS.
Param.- <data> Data to send.
Return.- void None
----------------------------------------------------------------*/
void MOD_LCD_Data(LCD_HandleTypeDef *hlcd, uint8_t data)
{
                       /*lcd_data mode RS = 1 */
    HAL_GPIO_WritePin(hlcd->RsPort, hlcd->RsPin, GPIO_PIN_SET);

                      /*chip select on CS = 0*/
    HAL_GPIO_WritePin(hlcd->CsPort, hlcd->CsPin, GPIO_PIN_RESET);
                      
                      /*Send SPI*/
    HAL_SPI_Transmit(hlcd->SpiHandler, &data, sizeof(data), 100);
                      
                      /*chip select off CS = 1 */
    HAL_GPIO_WritePin(hlcd->CsPort, hlcd->CsPin, GPIO_PIN_SET);
    
}


/*/*---------------------------------------------------------------
Brief.- Sends a string to the LCD.
Param.- <hlcd>  pointer to a LCD_HandleTypeDef structure that 
  contains the configuration SPI, GPIOS.
Param.- <str> pointer a string Data to send.
Return.- void None
----------------------------------------------------------------*/
void MOD_LCD_String(LCD_HandleTypeDef *hlcd, uint8_t *str)
{
   uint8_t size_str = strlen( (const char *) str);

   for(uint8_t i = 0; i < size_str; i++)
   {
      MOD_LCD_Data(hlcd, str[i]);
   }

}


/*/*---------------------------------------------------------------
Brief.- Set the cursor to the LCD.
Param.- <hlcd>  pointer to a LCD_HandleTypeDef structure that 
  contains the configuration SPI, GPIOS.
Param.- <row> Rows in the, can be   LCD 0-1
Param.- <col> Colums in the, can be LCD 0-15
Return.- void None
----------------------------------------------------------------*/
void MOD_LCD_SetCursor(LCD_HandleTypeDef *hlcd, uint8_t row, uint8_t col)
{
         /* Valide param row and Configure col */
      if( row == 0 )
      {
         MOD_LCD_Command(hlcd, LCD_INSTRUCTION_RETURN_HOME);
         MOD_LCD_Command(hlcd, LCD_INSTRUCTION_SET_DDRAM_MOVE_CURSOR(DDRAM_L0_Address[col]) );
      }else
      {
         MOD_LCD_Command(hlcd, LCD_INSTRUCTION_LINE_2);
         MOD_LCD_Command(hlcd, LCD_INSTRUCTION_SET_DDRAM_MOVE_CURSOR(DDRAM_L1_Address[col]) );
      } 
 
}