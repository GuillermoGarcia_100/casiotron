#include "app_bsp.h"

/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization System Clock Configuration
Param.- <void> None
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_MspInit(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Select HSI48 Oscillator as PLL source */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSI48State          = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI48;
  RCC_OscInitStruct.PLL.PREDIV          = RCC_PREDIV_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLL_MUL2;

  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  /* Select PLL as system clock source and configure the HCLK and PCLK1 clocks
   * dividers */
  RCC_ClkInitStruct.ClockType        = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1);
  RCC_ClkInitStruct.SYSCLKSource     = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider    = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider   = RCC_HCLK_DIV1;

  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);
}


/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization from RTC peripheral clock configuration
Param.- <hrtc> RTC handle
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_RTC_MspInit(RTC_HandleTypeDef *hrtc)
{
  /* System Clock Configuration LSE (32.768 KHz) */
  RCC_OscInitTypeDef RCC_Osc_InitStru;

  RCC_Osc_InitStru.OscillatorType    = RCC_OSCILLATORTYPE_LSE;
  RCC_Osc_InitStru.LSEState          = RCC_LSE_ON;
  RCC_Osc_InitStru.PLL.PLLSource     = RCC_PLL_NONE;

  HAL_RCC_OscConfig(&RCC_Osc_InitStru);

  __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSE); // Select source RTC, RTCSEL[1:0]

  __HAL_RCC_RTC_ENABLE();

  HAL_NVIC_SetPriority(RTC_IRQn, 1, 0);

  HAL_NVIC_EnableIRQ(RTC_IRQn);
}


/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization UART peripheral
Param.- <huart> UART handle
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  __HAL_RCC_USART2_CLK_ENABLE();

  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Init GPIOC Bottom User and LEDS */
  __HAL_RCC_GPIOC_CLK_ENABLE();

  GPIO_InitStruct.Pin       = GPIO_PIN_13;
  GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  /*  End Init Bottom User*/

  GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF1_USART2;

  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);

  HAL_NVIC_EnableIRQ(USART2_IRQn);
}


/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization SPI peripheral
Param.- <hspi> SPI handle
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
   GPIO_InitTypeDef  GPIO_InitStruct;
   
   /* ENABLE clock port GPIOB   (PB12 -> SPI2_NSS),  (PB13 -> SPI2_SCK),  (PB15 -> SPI2_MOSI)*/
   __HAL_RCC_GPIOB_CLK_ENABLE();
   __HAL_RCC_SPI2_CLK_ENABLE();

   /* Configure GPIO  */
   GPIO_InitStruct.Pin    =  GPIO_PIN_13 | GPIO_PIN_15;
   GPIO_InitStruct.Mode   =  GPIO_MODE_AF_PP;
   GPIO_InitStruct.Pull   =  GPIO_NOPULL;
   GPIO_InitStruct.Speed  =  GPIO_SPEED_FREQ_HIGH;
   GPIO_InitStruct.Alternate = GPIO_AF0_SPI2;

   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   GPIO_InitStruct.Pin    =  GPIO_PIN_12 | GPIO_PIN_3 | GPIO_PIN_4;
   GPIO_InitStruct.Mode   =  GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Pull   =  GPIO_NOPULL;
   GPIO_InitStruct.Speed  =  GPIO_SPEED_FREQ_HIGH;
    
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}


/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization WWDG peripheral
Param.- <hwwdg> WWDG handle
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_WWDG_MspInit(WWDG_HandleTypeDef *hwwdg)
{
  __HAL_RCC_WWDG_CLK_ENABLE();
}


/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization I2C peripheral
Param.- <hi2c> I2C handle
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void HAL_I2C_MspInit(I2C_HandleTypeDef *hi2c)
{

  GPIO_InitTypeDef  GPIO_InitStruct;
  RCC_PeriphCLKInitTypeDef  RCC_PeriphCLKInitStruct;
  
  /* Source Clock from I2C peripheral (SYSCLK) */
  RCC_PeriphCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  RCC_PeriphCLKInitStruct.I2c1ClockSelection   = RCC_I2C1CLKSOURCE_SYSCLK;

  HAL_RCCEx_PeriphCLKConfig(&RCC_PeriphCLKInitStruct);
  
  /* ENABLE clock port GPIOB   (PB6 -> I2C_SCL),  (PB7 -> I2C_SDA) */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_I2C1_CLK_ENABLE();

  GPIO_InitStruct.Pin       = GPIO_PIN_6 | GPIO_PIN_7;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF1_I2C1;

  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


}


/*/**---------------------------------------------------------------
Brief.- Function MSP Initialization I2C peripheral
Param.- <hi2c> I2C handle
Param.- <void> None
Return.- void None
----------------------------------------------------------------*/
void MOD_TEMP_MspInit(TEMP_HandleTypeDef *htemp)
{
    /*Init GPIOC Alert Pin  */
    GPIO_InitTypeDef GPIO_InitStruct;

    __HAL_RCC_GPIOA_CLK_ENABLE();

    GPIO_InitStruct.Pin       = htemp->AlertPin;
    GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  
   HAL_GPIO_Init(htemp->AlertPort, &GPIO_InitStruct);
   /*  End Init Alert Pin*/   

}