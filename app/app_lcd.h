/**------------------------------------------------------------------------------------------------
Brief.- lcd.h
2x16 alphanumeric LCD with spi interface
-------------------------------------------------------------------------------------------------*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _APP_LCD_H_
#define _APP_LCD_H_

/* Includes ------------------------------------------------------------------*/

#include "app_bsp.h"

/* Exported typedef -----------------------------------------------------------*/

typedef struct
{
    SPI_HandleTypeDef  *SpiHandler; /*!< SPI configure handler >*/

    GPIO_TypeDef       *RstPort;   /*!< GPIO configure Port   >*/

    uint16_t           RstPin;     /*!< GPIO configure PIN    >*/

    GPIO_TypeDef       *RsPort;    /*!< GPIO configure Port   >*/

    uint16_t           RsPin;     /*!< GPIO configure PIN     >*/

    GPIO_TypeDef       *CsPort;   /*!< GPIO configure Port    >*/

    uint16_t           CsPin;    /*!< GPIO configure PIN      >*/

} LCD_HandleTypeDef;

/* Exported define ------------------------------------------------------------*/

        /*    Instruction table at “Normal mode”        */
#define LCD_INSTRUCTION_CLEAR_DISPLAY                     0x01U  /*!<  Clear Display                                   >*/
#define LCD_INSTRUCTION_RETURN_HOME                       0x02U  /*!<  Return Home  Address 0x00                       >*/
#define LCD_INSTRUCTION_ENTRY_MODE                        0x06U  /*!<  Mode Set, Increment Address Counter, no shift   >*/
#define LCD_INSTRUCTION_DISPLAY_ON                        0x0CU  /*!<  Display ON,  No cursor, No Blinking             >*/
#define LCD_INSTRUCTION_DISPLAY_OFF                       0x08U  /*!<  Display OFF, No cursor, No Blinking             >*/
#define LCD_INSTRUCTION_DISPLAY_ON_P_CURSOR_ON            0x0EU  /*!<  Display ON, Yes cursor, No Blinking             >*/
#define LCD_INSTRUCTION_DISPLAY_ON_P_CURSOR_ON_BLINK_ON   0x0FU  /*!<  Display ON, Yes cursor, Yes Blinking            >*/        
#define LCD_INSTRUCTION_FUNCTION_SET_P_8BIT_2LINE_MODE    0x39U  /*!<  8 Bit Data, 2 Line Mode, Instruction Table 1     >*/
#define LCD_INSTRUCTION_SET_DDRAM_MOVE_CURSOR(X)          ( (128U) + (X) ) /*!<  Move the cursor  AC+1 Set DDRAM Address >*/

#define LCD_INSTRUCTION_SET_CGRAM                         0x40U  /*!<  CGRAM Address Set CGRAM add RESETs to 0x00       >*/
#define LCD_INSTRUCTION_WRITE_DATA_CGRAM_AND_END          0x38U  /*!<  CGRAM Write Select tabla 0, End write in the CGRAM >*/
#define LCD_INSTRUCTION_INTERNAL_OSC_P_FREQUENCY_183_HZ   0x14U  /*!<  1/5 Bias, 183 HZ                                 >*/
#define LCD_INSTRUCTION_POWER_CONTROL_P_BOOSTER_ON        0x56U  /*!<  Booster ON                                      >*/
#define LCD_INSTRUCTION_FOLLOWER_CONTROL_P_FOLLOWER_ON    0x6DU  /*!<  Follower ON, Follower Amplification Ratio 3:1   >*/                                
#define LCD_INSTRUCTION_CONTRAST_SET                      0x70U  /*!<  Contrast = 100000 32/63                        >*/

#define LCD_INSTRUCTION_LINE_2                            0xC0U  /*!<  Jump to Line 2                                 >*/
#define LCD_INSTRUCTION_WAKEUP                            0x30U  /*!<  Wakeup Controler                               >*/

/* Exported macro -------------------------------------------------------------*/
/* Exported variables ---------------------------------------------------------*/
/* Exported functions prototypes ----------------------------------------------*/

void MOD_LCD_Init(LCD_HandleTypeDef *hlcd);
void MOD_LCD_MspInit(LCD_HandleTypeDef *hlcd);
void MOD_LCD_Command(LCD_HandleTypeDef *hlcd, uint8_t cmd);
void MOD_LCD_Data(LCD_HandleTypeDef *hlcd, uint8_t data);
void MOD_LCD_String(LCD_HandleTypeDef *hlcd, uint8_t *str);
void MOD_LCD_SetCursor(LCD_HandleTypeDef *hlcd, uint8_t row, uint8_t col);

/*-----------------------------------------------------------------------------*/




#endif